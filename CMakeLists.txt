cmake_minimum_required(VERSION 3.13)
project(ocl_canny_edge_detection)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")

find_package(OpenCL REQUIRED)

include_directories(.)
include_directories(include)

add_executable(ocl_canny_edge_detection
        include/cxxopts.hpp
        cl_errorcheck.hpp
        imagelib.hpp
        io_helper.hpp
        main.cpp
        Makefile
        ocl_canny_edge_detection.pro
        ocl_helper.hpp
        ocl_source.cl)

target_link_libraries(ocl_canny_edge_detection OpenCL::OpenCL)