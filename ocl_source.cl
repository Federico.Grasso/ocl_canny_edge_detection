#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable

constant sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;
constant sampler_t sobel_sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;

#define R_LUMA_MULT 0.2126f
#define G_LUMA_MULT 0.7152f
#define B_LUMA_MULT 0.0722f

constant float filter_x_grad[9] = { // gradient horizontal
        1.0f,	2.0f,	1.0f,
        0.0f,	0.0f,	0.0f,
        -1.0f,	-2.0f,	-1.0f
};


constant float filter_y_grad[9] = { // gradient vertical
        1.0f,	0.0f,	-1.0f,
        2.0f,	0.0f,	-2.0f,
        1.0f,	0.0f,	-1.0f
};

void kernel make_luma_image(
    read_only image2d_t in_pic,
    write_only image2d_t luma_pic) {

    uint4 pixel = read_imageui(in_pic, (int2){get_global_id(0), get_global_id(1)}).x;
    uint luma = floor((R_LUMA_MULT * pixel.x) + (G_LUMA_MULT * pixel.y) + (B_LUMA_MULT * pixel.z));
    write_imageui(luma_pic, (int2){get_global_id(0), get_global_id(1)}, luma);
}

void kernel gaussian_blur(
        read_only image2d_t image,
        constant float * mask,
        write_only image2d_t image_out,
        private int maskSize
    ) {

    const int2 pos = {get_global_id(0), get_global_id(1)};

    // Collect neighbor values and multiply with gaussian
    float sum = 0.0f;
    // Calculate the mask size based on sigma (larger sigma, larger mask)
    for(int a = -maskSize; a < maskSize+1; a++) {
        for(int b = -maskSize; b < maskSize+1; b++) {
            sum += mask[a+maskSize+(b+maskSize)*(maskSize*2+1)]
                *read_imageui(image, sampler, pos + (int2)(a,b)).x;
        }
    }

    //blurredImage[pos.x+pos.y*get_global_size(0)] = sum;
    write_imageui(image_out, pos, sum);
}

void kernel sobel_filter_kernel(
    read_only image2d_t iimage, write_only image2d_t oimage, write_only image2d_t dirimage){
      const int2 pos = {get_global_id(0), get_global_id(1)};
      float gradientX = 0.0f;
      float gradientY = 0.0f;
      float computedGradient = 0.0f;
      float computedDirection = 0.0f;

      // Calculate the mask size based on sigma (larger sigma, larger mask)
      for(int a = 0; a < 3; a++) {
          for(int b = 0; b < 3; b++) {
              gradientX += filter_x_grad[a*3+b]
                  *read_imageui(iimage, sobel_sampler, pos + (int2)(a-1,b-1)).x;
              gradientY += filter_y_grad[a*3+b]
                  *read_imageui(iimage, sobel_sampler, pos + (int2)(a-1,b-1)).x;
          }
      }

      computedGradient = sqrt(gradientX*gradientX + gradientY*gradientY);
      computedDirection = (float)(fmod(atan2(gradientY, gradientX) + M_PI, M_PI) / M_PI) * 8;
      write_imageui(oimage, pos, computedGradient);
      write_imageui(dirimage, pos, computedDirection);
}


void kernel canny_filter_kernel(
    read_only image2d_t iimage, write_only image2d_t oimage, read_only image2d_t dirimage){
      const int2 pos = {get_global_id(0), get_global_id(1)};
      float computedValue = 0.0f;

      float i00 = read_imageui(iimage, sobel_sampler, pos + (int2)(-1,-1)).x;
      float i10 = read_imageui(iimage, sobel_sampler, pos + (int2)(-1,0)).x;
      float i20 = read_imageui(iimage, sobel_sampler, pos + (int2)(-1,1)).x;
      float i01 = read_imageui(iimage, sobel_sampler, pos + (int2)(0,-1)).x;
      float i11 = read_imageui(iimage, sobel_sampler, pos + (int2)(0,0)).x;
      float i21 = read_imageui(iimage, sobel_sampler, pos + (int2)(0,1)).x;
      float i02 = read_imageui(iimage, sobel_sampler, pos + (int2)(1,-1)).x;
      float i12 = read_imageui(iimage, sobel_sampler, pos + (int2)(1,0)).x;
      float i22 = read_imageui(iimage, sobel_sampler, pos + (int2)(1,1)).x;

      float d00 = read_imageui(dirimage, sobel_sampler, pos + (int2)(-1,-1)).x;
      float d10 = read_imageui(dirimage, sobel_sampler, pos + (int2)(-1,0)).x;
      float d20 = read_imageui(dirimage, sobel_sampler, pos + (int2)(-1,1)).x;
      float d01 = read_imageui(dirimage, sobel_sampler, pos + (int2)(0,-1)).x;
      float d11 = read_imageui(dirimage, sobel_sampler, pos + (int2)(0,0)).x;
      float d21 = read_imageui(dirimage, sobel_sampler, pos + (int2)(0,1)).x;
      float d02 = read_imageui(dirimage, sobel_sampler, pos + (int2)(1,-1)).x;
      float d12 = read_imageui(dirimage, sobel_sampler, pos + (int2)(1,0)).x;
      float d22 = read_imageui(dirimage, sobel_sampler, pos + (int2)(1,1)).x;

      if (((d11 <= 1 || d11 > 7) && i11 > i10 &&
           i11 > i12) || // 0 deg
          ((d11 > 1 && d11 <= 3) && i11 > i02 &&
           i11 > i20) || // 45 deg
          ((d11 > 3 && d11 <= 5) && i11 > i01 &&
           i11 > i21) || // 90 deg
          ((d11 > 5 && d11 <= 7) && i11 > i00 &&
           i11 > i22))   // 135 deg
          computedValue = i11;
      else
          computedValue = 0;

      write_imageui(oimage, pos, computedValue);
}

void kernel hyst_filter_kernel(
    read_only image2d_t iimage, write_only image2d_t oimage){
      const int2 pos = {get_global_id(0), get_global_id(1)};

      // Establish our high and low thresholds as floats
  	  float lowThresh = 10;
  	  float highThresh = 70;

      const float EDGE = 255.0f;

      float magnitude = read_imageui(iimage, sobel_sampler, pos).x;

      if (magnitude >= highThresh)
          write_imageui(oimage, pos, EDGE);
      else if (magnitude <= lowThresh)
          write_imageui(oimage, pos, 0);
      else
      {
          float med = (highThresh + lowThresh)/2;

          if (magnitude >= med)
              write_imageui(oimage, pos, EDGE);
          else
              write_imageui(oimage, pos, 0);
      }
}
