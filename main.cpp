#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <vector>
// These are for the random generation
#include <cstdlib>
#include <ctime>
// For log2f and floor/ceiling
#include <cmath>
#include <CL/cl.hpp>

#define DEBUG 1

#include "cl_errorcheck.hpp"
#include "include/cxxopts.hpp"
#include "imagelib.hpp"
#include "io_helper.hpp"
#include "ocl_helper.hpp"

int main(int argc, const char** argv) {

    std::string pwd = get_dir(argv[0]);

    cxxopts::Options options("ocl_canny_edge_detection", "OpenCL implementation of the Canny Edge Detection");
    options.add_options()
        ("p,profiling", "Enable profiling")
        ("i,input", "Input PPM image file path",
            cxxopts::value<std::string>())
        ("o,output", "Output file path",
            cxxopts::value<std::string>()->default_value(pwd + "/out.ppm"))
        ("l,localworksize", "Local work size",
            cxxopts::value<int>()->default_value("0"))
        ("a,automaton", "Automaton implementation (valid values: global, local, image)\n\tglobal: use global memory\n\tlocal: use local memory\n\timage: use texture memory",
            cxxopts::value<std::string>()->default_value("global"))
        ("P,selectplatform", "Manually select platform in runtime",
            cxxopts::value<int>()->default_value("0"));



    auto result = options.parse(argc, argv);

    std::string bmp_path="";
    std::string out_path="";

    if (result.count("i") == 1) {
        bmp_path = result["i"].as<std::string>();
    }
    else {
        std::cerr << options.help() << std::endl;
        exit(1);
    }

    int lws_cli = result["l"].as<int>();
    std::string automaton_memory = result["a"].as<std::string>();
    if (automaton_memory != "global" && automaton_memory != "local" && automaton_memory != "image") {
        std::cerr <<
            "WARNING: provided automaton implementation argument (-a, --automaton) invalid. Falling back to global" <<
            std::endl;
        automaton_memory = "global";
    }

    out_path = result["o"].as<std::string>();
    bool enable_profiling = result.count("p");
    int selectplatform = result["P"].as<int>();

    cl_int err;

    BMPVEC bmp;

    int bmp_width;
    int bmp_height;

    read_ppm(bmp_path, bmp, bmp_width, bmp_height);

    std::cout <<
             "{\n\"image\": \"" <<
             bmp_path <<
             "\"," << std::endl <<
             "\"image_size\": \"" <<
             bmp_width << "x" << bmp_height << "\"," <<
             std::endl;


    BMPVEC bmp_RGBA_data;
    bgr2bgra(bmp, bmp_RGBA_data);

    cl::Device default_device = ocl_get_default_device(selectplatform);
    cl::Context context({default_device});
    cl::Program::Sources sources;

    cl::CommandQueue queue;
    if (enable_profiling) queue = cl::CommandQueue(context, default_device, CL_QUEUE_PROFILING_ENABLE);
    else queue = cl::CommandQueue(context, default_device);

    cl::Image2D cl_input_image =  cl::Image2D(
                context,
                CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
                cl::ImageFormat(CL_RGBA, CL_UNSIGNED_INT8),
                bmp_width, bmp_height,
                0,
                (void*)(&bmp_RGBA_data[0]),
                &err);
    cl_check(err, "Creating input image");

    cl::Image2D cl_luma_image = cl::Image2D(
                context,
                CL_MEM_READ_WRITE,
                cl::ImageFormat(CL_R, CL_UNSIGNED_INT8),
                bmp_width, bmp_height,
                0,
                NULL,
                &err);
    cl_check(err, "Creating luma image");

    cl::Image2D cl_gaussian_image = cl::Image2D(
                context,
                CL_MEM_READ_WRITE,
                cl::ImageFormat(CL_R, CL_UNSIGNED_INT8),
                bmp_width, bmp_height,
                0,
                NULL,
                &err);
    cl_check(err, "Creating gaussian image");

    cl::Image2D cl_sobel_image = cl::Image2D(
                context,
                CL_MEM_READ_WRITE,
                cl::ImageFormat(CL_R, CL_UNSIGNED_INT8),
                bmp_width, bmp_height,
                0,
                NULL,
                &err);
    cl_check(err, "Creating sobel image");

    cl::Image2D cl_direction_image = cl::Image2D(
                context,
                CL_MEM_READ_WRITE,
                cl::ImageFormat(CL_R, CL_UNSIGNED_INT8),
                bmp_width, bmp_height,
                0,
                NULL,
                &err);
    cl_check(err, "Creating direction image");

    cl::Image2D cl_hyst_image = cl::Image2D(
                context,
                CL_MEM_READ_WRITE,
                cl::ImageFormat(CL_R, CL_UNSIGNED_INT8),
                bmp_width, bmp_height,
                0,
                NULL,
                &err);
    cl_check(err, "Creating Hyst image");

    cl::Image2D cl_output_image = cl::Image2D(
                context,
                CL_MEM_WRITE_ONLY,
                cl::ImageFormat(CL_RGBA, CL_UNSIGNED_INT8),
                bmp_width, bmp_height,
                0,
                NULL,
                &err);
    cl_check(err, "Creating output image");

    uint32_t* host_init_are_diff = new uint32_t();
    host_init_are_diff[0] = 0u;
    cl::Buffer cl_are_diff(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(cl_uint), host_init_are_diff, &err);

    cl_check(err, "Creating are_diff value buffer");

    // Create Gaussian mask
    int maskSize;
    float sigma = 1.0f;
    float * mask = createBlurMask(sigma, &maskSize);
    cl::Buffer clMask(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float)*(maskSize*2+1)*(maskSize*2+1), mask);

    std::string ocl_source = read_kernel(pwd + "/ocl_source.cl");
    sources.push_back({ocl_source.c_str(), ocl_source.length()});
    cl::Program program(context, sources);
    if (program.build({default_device}) != CL_SUCCESS) {
        std::cerr <<
                     "Error Building: " <<
                     program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(default_device) <<
                     std::endl;
        exit(1);
    }

#if 0
    std::cout << "Program build log:\n" <<
        program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(default_device) <<
        std::endl << std::endl;
#endif

    cl::Kernel kernel_make_luma_image = cl::Kernel(program, "make_luma_image");

    kernel_make_luma_image.setArg(0, cl_input_image);
    kernel_make_luma_image.setArg(1, cl_luma_image);

    queue.enqueueNDRangeKernel(
            kernel_make_luma_image,
            cl::NullRange,
            cl::NDRange(bmp_width, bmp_height),
            cl::NullRange);

    //queue.finish();

    cl::Kernel gaussianBlur = cl::Kernel(program, "gaussian_blur");

    gaussianBlur.setArg(0, cl_luma_image);
    gaussianBlur.setArg(1, clMask);
    gaussianBlur.setArg(2, cl_gaussian_image);
    gaussianBlur.setArg(3, maskSize);

    queue.enqueueNDRangeKernel(
            gaussianBlur,
            cl::NullRange,
            cl::NDRange(bmp_width, bmp_height),
            cl::NullRange);

    //queue.finish();

    cl::Kernel sobelFilter = cl::Kernel(program, "sobel_filter_kernel");

    sobelFilter.setArg(0, cl_gaussian_image);
    sobelFilter.setArg(1, cl_sobel_image);
    sobelFilter.setArg(2, cl_direction_image);

    queue.enqueueNDRangeKernel(
            sobelFilter,
            cl::NullRange,
            cl::NDRange(bmp_width, bmp_height),
            cl::NullRange);

    //queue.finish();

    cl::Kernel cannyFilter = cl::Kernel(program, "canny_filter_kernel");

    cannyFilter.setArg(0, cl_sobel_image);
    cannyFilter.setArg(1, cl_hyst_image);
    cannyFilter.setArg(2, cl_direction_image);

    queue.enqueueNDRangeKernel(
            cannyFilter,
            cl::NullRange,
            cl::NDRange(bmp_width, bmp_height),
            cl::NullRange);

    //queue.finish();

    cl::Kernel hystFilter = cl::Kernel(program, "hyst_filter_kernel");

    hystFilter.setArg(0, cl_hyst_image);
    hystFilter.setArg(1, cl_output_image);

    queue.enqueueNDRangeKernel(
            hystFilter,
            cl::NullRange,
            cl::NDRange(bmp_width, bmp_height),
            cl::NullRange);

    //queue.finish();

    if (automaton_memory == "global" || automaton_memory == "local") {

        queue.finish();

    }

    cl::NDRange gmem_local_ndrange;
    cl_int gmem_gws_width;
    cl_int gmem_gws_height;
    cl_int gmem_lws;

    // Preferred Group Size Multiple
    cl_int pref_gs_mult = gaussianBlur.getWorkGroupInfo<CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE>(
            default_device,
            &err);
    cl_check(err, "Getting preferred group size multiple");


    if (automaton_memory == "global" || automaton_memory == "image") {

        if (lws_cli) {

            gmem_lws = lws_cli ? lws_cli : pref_gs_mult;
            gmem_gws_width = round_up(bmp_width, gmem_lws);
            gmem_gws_height = round_up(bmp_height, gmem_lws);
            gmem_local_ndrange = cl::NDRange(gmem_lws, gmem_lws);
        }
        else {
            gmem_local_ndrange = cl::NullRange;
            gmem_gws_width = round_up(bmp_width, pref_gs_mult);
            gmem_gws_height = round_up(bmp_height, pref_gs_mult);
        }

    }

    std::cout <<
        "\"implementation\": \"" << automaton_memory <<
        "\"," << std::endl <<
        "\"size\": \"" << (lws_cli ? lws_cli : 0) <<
        "\"" << std::endl <<
        "}" << std::endl;

    uint8_t* host_outimage = new uint8_t[bmp_width*bmp_height*4];
    uint8_t* rgb_outimage = new uint8_t[bmp_width*bmp_height*3];

    cl::size_t<3> ri_origin;
    ri_origin[0] = 0;
    ri_origin[1] = 0;
    ri_origin[2] = 0;
    cl::size_t<3> ri_region;
    ri_region[0] = bmp_width;
    ri_region[1] = bmp_height;
    ri_region[2] = 1;
    err = queue.enqueueReadImage(
                        cl_output_image,
                        CL_TRUE,
                        ri_origin,
                        ri_region,
                        0,
                        0,
                        host_outimage);
    cl_check(err, "Reading image from device");

    rgba2rgb(
        host_outimage,
        bmp_width*bmp_height,
        rgb_outimage
    );

    write_ppm(rgb_outimage,
        3*bmp_width*bmp_height,
        bmp_width,
        bmp_height,
        out_path);

    return 0;
}
